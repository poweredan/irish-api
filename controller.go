package main

import (
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func parseTemplate(name string, path string, baseTemplate *template.Template) {
	fileBytes, err := read(path)
	if err == nil {
		_, err = baseTemplate.New(name).Parse(string(fileBytes))
		check(err)
	} else {
		log.Println(err)
	}
}

func controller(path string, bytes []byte, w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	templateData := make(map[string]interface{})
	templateData["params"] = params
	baseTemplate := template.New("all")
	var err error
	if _, err = baseTemplate.New("base").Parse(string(bytes)); err != nil {
		log.Fatal(err)
	}
	parseTemplate("head", "app/partials/head.html", baseTemplate)
	parseTemplate("header", "app/partials/header.html", baseTemplate)
	switch path {
	case "/":
		var session string
		for _, cookie := range r.Cookies() {
			if cookie.Name == "session" {
				session = cookie.Value
			}
		}
		_ = session
	case "/get-image":
		var (
			number int
			umarks int
		)
		clientId := params["client_id"][0]
		db.Get("umarks."+clientId, &umarks)
		mark := umarks > 19
		if mark {
			umarks = 0
			number, err = strconv.Atoi(params["range_extraction"][0])
			check(err)
		} else {
			umarks++
			number = rand.Intn(100)
		}
		db.Set("umarks."+clientId, &umarks)
		var numberCp int
		if mark {
			if number > 50 {
				numberCp = 5
			} else {
				numberCp = 95
			}
		} else {
			numberCp = number
		}
		numberS := strconv.Itoa(number)
		numberCpS := strconv.Itoa(numberCp)
		if number < 10 {
			numberS = "0" + numberS
		}
		if numberCp < 10 {
			numberCpS = "0" + numberCpS
		}
		timestampDirs, err := filepath.Glob("public/1561*")
		check(err)
		timestamps := map[string]bool{}
		db.Get(clientId, &timestamps)
		var timestamp string
		for i, timestampDir := range timestampDirs {
			timestamp = strings.Split(timestampDir, string(os.PathSeparator))[1]
			if mark {
				if !timestamps[timestamp] {
					break
				}
			} else {
				if i == rand.Intn(len(timestampDirs)) {
					break
				}
			}
		}
		log.Println(timestamp)
		suffix := map[bool]string{false: ".1", true: ".2"}[mark]
		readBytes, _ := read("public/" + timestamp + "/" + numberS + suffix + ".jpg")
		setContentTypeHeader(w, "jpg")
		w.Header().Add("X-Number", numberCpS)
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Access-Control-Expose-Headers", "x-number")
		if _, err := w.Write(readBytes); err != nil {
			log.Println(err)
		}
		if mark {
			timestamps[timestamp] = true
			db.Set(clientId, &timestamps)
		}
	}
	err = baseTemplate.ExecuteTemplate(w, "base", templateData)
	check(err)
}
