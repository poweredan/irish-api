package main

import (
	"fmt"
	"github.com/recoilme/pudge"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

var (
	routes               map[string]string
	db                   *pudge.Db
	err                  error
	cacheLock            = sync.RWMutex{}
	cache                = make(map[string][]byte)
	controlled           = map[string]bool{"html": true, "api": true}
	syncFSUpdateInterval = 1000 * time.Millisecond
	syncFSSleepInterval  = 900 * time.Millisecond
	addr                 = ":4434"
)

func readRoutes() {
	routesBytes, err := read("routes")
	if err != nil {
		log.Panic(err)
	}
	routes = parseConfig(string(routesBytes))
}

func setContentTypeHeader(w http.ResponseWriter, extension string) {
	var mime string
	switch extension {
	case "css":
		mime = "text/css; charset=utf-8"
	case "js":
		mime = "text/javascript"
	case "svg":
		mime = "image/svg+xml"
	case "html":
		mime = "text/html"
	case "htm":
		mime = "text/html"
	case "png":
		mime = "image/png"
	case "jpg":
		mime = "image/jpeg"
	case "jpeg":
		mime = "image/jpeg"
	case "woff":
		mime = "font/woff"
	case "woff2":
		mime = "font/woff2"
	case "eot":
		mime = "application/vnd.ms-fontobject"
	case "ttf":
		mime = "application/font-sfnt"
	case "webmanifest":
		mime = "text/plain"
	case "txt":
		mime = "text/plain"
	case "xml":
		mime = "text/xml"
	default:
		return
	}
	w.Header().Add("Content-Type", mime)
}

func lastString(stringsSlice []string) string {
	return stringsSlice[len(stringsSlice)-1]
}

func fileExists(path string) bool {
	_, err = os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func read(path string) ([]byte, error) {
	if cached, ok := cache[path]; ok {
		fmt.Printf("reading from cache: %s\r\n", path)
		return cached, nil
	} else {
		fmt.Printf("reading from disk: %s\r\n", path)
	}
	var htmlBytes []byte
	if fileExists(path) {
		htmlBytes, err = ioutil.ReadFile(path)
		if err != nil {
			return htmlBytes, err
		}
		cacheLock.Lock()
		cache[path] = htmlBytes
		cacheLock.Unlock()
	} else {
		return htmlBytes, fmt.Errorf("файл не существует։ %s", path)
	}
	return htmlBytes, nil
}

func parseConfig(linesString string) map[string]string {
	result := make(map[string]string)
	lines := strings.Split(linesString, "\r\n")
	for _, line := range lines {
		keyValue := strings.Split(line, " ")
		key := keyValue[0]
		if key != "" {
			result[key] = keyValue[1]
		}
	}
	return result
}

func syncFS() {
	for {
		for k := range cache {
			fi, err := os.Stat(k)
			check(err)
			modTime := fi.ModTime()
			sec := time.Now().Sub(modTime)
			if sec < syncFSUpdateInterval {
				log.Printf("updating file cache for: %s", k)
				htmlBytes, err := ioutil.ReadFile(k)
				if err == nil {
					cacheLock.Lock()
					cache[k] = htmlBytes
					cacheLock.Unlock()
				} else {
					log.Print(err)
				}
			}
		}
		time.Sleep(syncFSSleepInterval)
	}
}

func initDb() {
	db, err = pudge.Open("client_timestamps.db", &pudge.Config{SyncInterval: 1})
	check(err)
}

func check(err error) {
	if err != nil {
		log.Println(err)
	}
}

func mainHandler(w http.ResponseWriter, r *http.Request) {
	var route string
	reqPath := r.URL.Path
	if strings.HasPrefix(reqPath, "/public/") {
		route = strings.TrimLeft(reqPath, "/")
	} else {
		route = routes[reqPath]
	}
	readBytes, err := read(route)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		_, err := w.Write([]byte("Resource or file not found 404"))
		check(err)
		return
	}
	extension := lastString(strings.Split(route, "."))
	switch true {
	case controlled[extension]:
		fmt.Printf("New HTML Request, path: %s\r\n", reqPath)
		controller(reqPath, readBytes, w, r)
	default:
		setContentTypeHeader(w, extension)
		w.Header().Add("Cache-Control", "max-age=31536000")
		if _, err = w.Write(readBytes); err != nil {
			log.Println(err)
		}
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	go syncFS()
	defer pudge.CloseAll()
	go initDb()
	readRoutes()
	http.HandleFunc("/", mainHandler)
	err = http.ListenAndServeTLS(addr, "ssl/irish-api.ga.crt", "ssl/irish-api.ga.key", nil)
	if err != nil {
		log.Fatal(err)
	}
}
